/**
 * Created by namax on 03/03/17.
 */
class Polygon {

    constructor(height, width) {
        this.name = 'Polygon';
        this.height = height;
        this.width = width;
    }

    // Simple class instance methods using short-hand method
    // declaration
    sayName() {
        console.log('Hi, I am a CUSTOM2 ' + this.name + '.');
    }

    sayHistory() {
        console.log('"Polygon" is derived from the Greek polus (many) ' +
            'and gonia (angle).');
    }

    // We will look at static and subclassed methods shortly
}

export default Polygon;