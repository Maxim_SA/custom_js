/**
 * Created by namax on 02/03/17.
 */

function buildConfig(env) {
    return require('./config/' + env + '.js')({ env: env })
}

module.exports = buildConfig;