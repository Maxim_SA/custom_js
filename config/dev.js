/**
 * Created by namax on 07/03/17.
 */
const path = require('path');
const webpackMerge = require('webpack-merge');
const rootDir = path.resolve(__dirname, "../../../../");
const customDir = path.resolve(rootDir, "custom/");
const commonConfig = require(path.resolve(rootDir, "./_resources/js/config/base.js"));

module.exports = function(env) {
    return webpackMerge(commonConfig(), {
        devtool: 'cheap-module-source-map',
        resolve: {
            extensions: ['.js', '.json'],
            modules: [
                path.join(rootDir, 'node_modules'),
                path.join(rootDir, '_resources/js/src')
            ],
            alias: {
                // manual_clerking_hybrid_settings: path.resolve(rootDir, "wwwroot/assets/js/admin/manual_clerking_hybrid_settings.js"),
                Polygon: path.resolve(customDir, "./_resources/js/src/admin/Polygon.js"),
            }
        },
    })
}
